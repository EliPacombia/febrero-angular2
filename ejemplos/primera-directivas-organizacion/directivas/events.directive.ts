import { Directive, Input,ElementRef,Renderer } from '@angular/core';

@Directive({
  selector: '[EventsDirective]'
})
export class EventsDirective
 {
   constructor(el:ElementRef,renderer:Renderer){
     renderer.setElementStyle(el.nativeElement,'color','#EF997D');
     renderer.setElementStyle(el.nativeElement,'textDecoration','underline');
   }
}
