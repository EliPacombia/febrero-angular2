import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//pipes
import {JoinPipes} from './pipes/join/join.pipes';

//directiva
import {EventsDirective} from './directivas/events.directive';

import { HomeComponent } from './components/home/home.components';


@NgModule({
  declarations: [
    HomeComponent,
    EventsDirective,
    JoinPipes

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class AppModule { }
