import {Component} from '@angular/core';

@Component({
  selector:'app-root',
  template:`
  <h1> {{title | lowercase}}</h1>
  <p> {{paragraph | uppercase}}</p>
  <p> {{today | date:"dd/MM/yy"}}</p>
  <pre> {{users | json}} </pre>
  `
})

export class HomeComponent
{
title:string = "pipes de Angualar 2";
paragraph:string = "Paragrafo";
today:Date = new Date;
users=['aparra','silvia','juan']
}
