import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//directiva
import {EventsDirective} from './directivas/events.directive';

import { HomeComponent } from './components/home/home.components';


@NgModule({
  declarations: [
    HomeComponent,
    EventsDirective

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class AppModule { }
