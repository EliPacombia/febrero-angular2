import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'panel',
  template: `
  <div class="card-panel">
  <span class="black-text">
    UserId:{{user.id}},username:{{user.username}}
    </span>
  `,
})
export class PanelComponent implements OnInit {
  constructor() {  }
  user:object={
    id:1,
    username:'Pacombia'
  }
  ngOnInit() {}
}
