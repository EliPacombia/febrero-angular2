import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div class="container">
    <panel></panel>
    </div>
  `
})

export class HomeComponent {
  user: Object = {
    id: 1,
    username: 'Pacombia',
    age: 35
  }
  displayUser(user: Object): void{
    console.log(user);
  }
}
