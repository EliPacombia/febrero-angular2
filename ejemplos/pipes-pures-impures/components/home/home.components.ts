import {Component} from '@angular/core';

@Component({
  selector:'app-root',
  template:`
  <h2> Pipes puros e Impuros en Angular 2 </h2>
  <input type="text" [(ngModel)]="username" />
  {{username | pure_impure}}
  `
})

export class HomeComponent
{
  username:string = "iparra";
}
