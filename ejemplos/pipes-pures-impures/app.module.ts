import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';


//pipes
import {JoinPipes} from './pipes/join/join.pipes';
import {PureImpurePipe} from './pipes/pure_impure.pipes';

//directiva
import {EventsDirective} from './directivas/events.directive';

import { HomeComponent } from './components/home/home.components';


@NgModule({
  declarations: [
    HomeComponent,
    EventsDirective,
    JoinPipes,
    PureImpurePipe

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class AppModule { }
