import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <p>(UNIDIRECIONAL CLASE A TEMPLATE)=>
  UserId: {{user.id}},
  username: {{user.username}},
  Age: {{user.age}}
  </p>
  <hr>
  <p>(UNIDIRECIONAL CLASE A TEMPLATE)=>
  <span (click)="displayUser(user)">Click para mostrar usuario</span></p>
    <hr>
  <p>(BIDIRECCINAL)=> <input [(ngModel)]="user.username"/></p>
  `
})

export class HomeComponent {
  user: Object = {
    id: 1,
    username: 'Pacombia',
    age: 35
  }
  displayUser(user: Object): void{
    console.log(user);
  }
}
