import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div class="container">
    <panel [user]="user" (onSelect)="selectUser($event)"></panel>
    </div>
  `
})

export class HomeComponent
{
  user:Object={
    id:1,
    username:"Pacombia",
    age:26
  }
  selectUser(selected){
    console.log("evento lanzado, select es: " + selected);
  }
}
